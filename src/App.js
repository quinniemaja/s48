// import {Fragment} from 'react'
import {Container} from 'react-bootstrap'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'
import AppNavbar from './components/AppNavbar'
import Courses from './pages/Courses'
import ErrorPage from './pages/ErrorPage'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Register from './pages/Register'

import './App.css';

function App() {
  return (
   <Router>
   	<AppNavbar/>
   	<Container>
   		<Switch>
   			<Route exact path = "/" component = {Home}/>
   			<Route exact path = "/courses" component = {Courses}/>
   			<Route exact path = "/login" component = {Login}/>
   			<Route exact path = "/register" component = {Register}/>
   			<Route exact path = "/logout" component = {Logout}/>
        <Route exact path = "/" component = {Logout}/>
        <Route exact path = "*" component = {ErrorPage}/>
   		</Switch>
   	</Container>
   </Router>
  );
}

export default App;


/*
	ReactJS is a single page application (SPA). However , we can simulate the changing of pages. We don't actually create new pages, what we just do is switch pages to their assigned routes. ReactJS and react-router-dom package mimics or mirrors how HTML access its URL. 


	Rreact-router-dom 
		3 main components to simulate the switching of pages.
			1. Router - wrapping the router components around other components will allow us to use routing within our page.
			2. Switch - allow us to switch change our page ocmponents
			3. Route - assigns paths which will trigger the change/switch of compnents render.
*/