import {useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap'


export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function loginUser(e) {

		e.preventDefault();

		// set the user's email to the local storage
		// syntax: localStorage.setItem('propertyName, value')

		localStorage.setItem('email', email);

		setEmail('');
		setPassword('');

		alert('You are now logged in.')
	}

	useEffect(() => {
		if(email !== '' && password !== '') {

			setIsActive(true)

		} else {

			setIsActive(false)
		}
	}, [email, password])

	return (

		<Form onSubmit = {(e) => loginUser(e)}>
			<h2>Login</h2>
			<Form.Group>
				<Form.Label>Email Address:</Form.Label>
				<Form.Control 
					type = 'email'
					placeholder = 'Please enter your email here'
					value = {email}
					onChange = {e => setEmail(e.target.value)}
					required/>
				<Form.Text className = "text-muted">
					We'll never share your email with anyone else.
				</Form.Text>	
			</Form.Group>

			<Form.Group controlId = "password1">
				<Form.Label>Password:</Form.Label>
				<Form.Control 
					type = 'password'
					placeholder = 'Please input your password'
					value = {password}
					onChange = {e => setPassword(e.target.value)}
					required/>
			</Form.Group>
			<br></br>

			{ isActive ? // if statement
				<Button variant = 'primary' type = 'submit' id = 'submitBtn'>
							Login
				</Button>

				: // stands for else 
				<Button variant = 'danger' type = 'submit' id= 'submitBtn' disabled>
					Login
				</Button>
			}
		</Form>

	)
}